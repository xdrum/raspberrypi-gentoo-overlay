# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit git-2

DESCRIPTION="Pre-compiled binaries and firmwares for the Raspberry Pi"
HOMEPAGE="http://www.raspberrypi.org/"
EGIT_REPO_URI="git://github.com/raspberrypi/firmware.git"
EGIT_COMMIT="467b0b45e15c6f18047e09d3e7f3d3efb5653b09"

LICENSE="Broadcom"
SLOT="0"
KEYWORDS="~arm"
IUSE="kernels"

src_install() {
	cd "${S}/boot"
	insinto /boot
	newins "${S}/boot/arm192_start.elf" start.elf || die
	local each
	for each in arm*_start.elf *.bin; do
		doins ${each} || die
	done

	if use kernels ; then
		doins kernel*.img

		cd "${S}/modules"
		insinto /lib/modules
		doins -r 3.1.9* || die
	fi
}

pkg_postinst() {
	einfo "*** BIG FAT WARNING ***"
	einfo ""
	einfo "This is an unofficial ebuild, for info and support see:"
	einfo "https://github.com/raspberrypi/firmware"
	einfo ""
	einfo "Additional GPU firmware images are present in: ${ROOT}boot"
	einfo "copy over to use them, eg:"
	einfo "cp ${ROOT}boot/arm128_start.elf ${ROOT}boot/start.elf"
	einfo "* arm128_start.elf : 128M ARM, 128M GPU split (use fpr heavy 3D work)"
	einfo "* arm192_start.elf : 192M ARM, 64M GPU split (this is the default)"
	einfo "* arm224_start.elf : 224M ARM, 32M GPU split (no 3D/video processing)"
	if use kernels; then
		einfo ""
		einfo "Additional binary kernels images and modules are present in:"
		einfo "${ROOT}boot and ${ROOT}lib/modules"
		einfo "copy over to use them, eg:"
		einfo "cp ${ROOT}boot/kernel_emergency.img ${ROOT}boot/kernel.img"
		einfo "(ensure you have a backup of the original kernel.img first!)"
	fi
}
