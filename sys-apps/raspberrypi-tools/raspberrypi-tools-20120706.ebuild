# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit eutils git-2

DESCRIPTION="Raspberry Pi system image development kit"
HOMEPAGE="http://www.raspberrypi.org/"

EGIT_REPO_URI="git://github.com/raspberrypi/tools.git"
EGIT_COMMIT="772201f23f75e6fc345d55627d4973893c440f7c"

# I was not able to figure out what license applies to this tool..
LICENSE=""
SLOT="0"

KEYWORDS="~arm"
IUSE=""

RDEPEND="${RDEPEND}
	>=dev-lang/python-2.5"

src_prepare() {
	epatch "${FILESDIR}/${PN}-mkimage-paths.patch"
}

src_install() {
	cd "${S}/mkimage"
	insinto /usr/share/raspberrypi-tools/mkimage
	doins *uncompressed.txt first32k.bin

	exeinto /usr/bin
	newexe imagetool-uncompressed.py raspberrypi-mkimage
}

pkg_postinst() {
	einfo "*** BIG FAT WARNING ***"
	einfo ""
	einfo "This is an unofficial ebuild, for info and support see:"
	einfo "https://github.com/raspberrypi/tools"
}
