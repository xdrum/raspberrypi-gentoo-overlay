# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
ETYPE="sources"
inherit kernel-2 git-2
detect_version
detect_arch

HOMEPAGE="http://www.raspberrypi.org/ http://www.kernel.org"
DESCRIPTION="Linux kernel sources patched for Raspberry Pi"

EGIT_REPO_URI="git://github.com/raspberrypi/linux.git"
EGIT_COMMIT="85b7821857dd0b9cabab59d47f08eabed74679a3"
EGIT_BRANCH="rpi-patches"

KEYWORDS="~arm"
IUSE=""

src_install() {
	kernel-2_src_install
}

pkg_postinst() {
	kernel-2_pkg_postinst
	einfo "*** BIG FAT WARNING ***"
	einfo ""
	einfo "This is an unofficial ebuild, for info and support see:"
	einfo "https://github.com/raspberrypi/linux/issues"
}
